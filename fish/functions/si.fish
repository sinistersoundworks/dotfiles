function si --wraps='sox --info' --description 'alias si=sox --info'
  sox --info $argv
        
end
