function pycharm --wraps='/Applications/PyCharm CE.app/Contents/MacOS/pycharm' --wraps=/Applications/PyCharm\\\ CE.app/Contents/MacOS/pycharm --description alias\ pycharm=/Applications/PyCharm\\\ CE.app/Contents/MacOS/pycharm
  /Applications/PyCharm\ CE.app/Contents/MacOS/pycharm $argv
        
end
