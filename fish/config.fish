# Mute greeting
set fish_greeting

# Silence Homebrew
set -g HOMEBREW_NO_ENV_HINTS 1

# Set common defaults
set LC_ALL en_US.UTF8
set LANG en_US.UTF8

if status is-interactive
    # Commands to run in interactive sessions can go here
    set EDITOR /opt/homebrew/bin/nvim
    set VISUAL /opt/homebrew/bin/nvim
    set TERM xterm-256color
end

# >>> conda initialize >>>
eval "$(/Volumes/Storage/miniforge3/bin/conda shell.fish hook)"
# <<< conda initialize <<<

# pnpm
set PNPM_HOME /Users/gz/Library/pnpm
set PATH "$PNPM_HOME:$PATH"
set PATH "$HOME/.cargo/bin:$HOME/.local/bin:$PATH"
set PATH "/Applications/PyCharm\ CE.app/Contents/MacOS:$PATH"
# pnpm end

#eval "$(ssh-agent)"
#if test -z (pgrep ssh-agent)
#    eval (ssh-agent -c)
#    set -Ux SSH_AUTH_SOCK $SSH_AUTH_SOCK
#    set -Ux SSH_AGENT_PID $SSH_AGENT_PID
#    set -Ux SSH_AUTH_SOCK $SSH_AUTH_SOCK
#end

thefuck --alias | source

# Pyenv init
#pyenv init - | source

set PATH "$HOME/scripts:$PATH"

### Functions
# Bang-bang function (does not work in Vi mode)
function __history_previous_command
  switch (commandline -t)
	case "!"
		commandline -t $history[1]; commandline -f repaint
	case "*"
		commandline -i !
	end
end
## !$ Function
function __history_previous_command_arguments
  switch (commandline -t)
	case "!"
		commandline -t ""
		commandline -f history-token-search-backward
	case "*"
		commandline -i '$'
	end
end
# Keybindings for !! and !$
bind ! __history_previous_command
bind '$' __history_previous_command_arguments


test -e {$HOME}/.iterm2_shell_integration.fish ; and source {$HOME}/.iterm2_shell_integration.fish

